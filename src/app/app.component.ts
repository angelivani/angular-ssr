import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { PlatformService } from './../shared/services/platform.service';
import { SocialMetaService } from './../shared/services/social-meta.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  public angularSsr = 'http://157.230.32.82:4000';

  public socialMediaType: any = {
    facebook: 'facebook',
    twitter: 'twitter',
    whatsapp: 'whatsapp',
    line: 'line',
    telegram: 'telegram'
  };

  public socialMediaShareEndPoint: any = {
    facebook: 'https://www.facebook.com/sharer/sharer.php',
    twitter: 'https://twitter.com/intent/tweet',
    whatsapp: 'https://web.whatsapp.com/send',
    line: 'https://social-plugins.line.me/lineit/share',
    telegram: 'https://t.me/share/url'
  };

  constructor(
    private platform: PlatformService,
    private socialMeta: SocialMetaService,
    private http: HttpClient
  ) {
    if (this.platform.isServer) {
      this.http.get('https://97d709d4-cdcd-4e37-847a-f6f86b070b99.mock.pstmn.io/image/uploaded').subscribe(
        (res: any) => {
          console.log(res);
          
          this.socialMeta.setFacebookMeta({
            imagePath: res.link
          });
        },
        (err: any) => {
          console.log(err);
          return err;
        }
      );

    }
    else if(this.platform.isBrowser) {
      console.log('YES IT IS BROWSER');
    }
  }

  ngOnInit() {}

  private popupwindow(url: string, width: number, height: number) {
    const left = screen.width / 2 - width / 2;
    const top = screen.height / 2 - height / 2;
    return window.open(
      url,
      '',
      'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width=' +
        width +
        ', height=' +
        height +
        ', top=' +
        top +
        ', left=' +
        left,
    );
  }

  private getShareURL(socialMedia: string, paramShareURL: string) {
    return `${this.socialMediaShareEndPoint[socialMedia]}${paramShareURL}`;
  }

  public shareViaSocialMedia(mediaType: any) {
    const encodedURL = encodeURIComponent(this.angularSsr);

    let paramShareURL = '';
    switch (mediaType) {
      case this.socialMediaType.facebook: {
        paramShareURL = `?u=${encodedURL}`;
        break;
      }
      case this.socialMediaType.twitter: {
        paramShareURL = `?url=${encodedURL}`;
        break;
      }
      case this.socialMediaType.whatsapp: {
        paramShareURL = `?text=${encodedURL}`;
        break;
      }
      case this.socialMediaType.line: {
        paramShareURL = `?url=${encodedURL}`;
        break;
      }
      case this.socialMediaType.telegram: {
        paramShareURL = `?url=${encodedURL}`;
        break;
      }
    }
    const shareURL = this.getShareURL(mediaType, paramShareURL);
    this.popupwindow(shareURL, 300, 400);
  }
}
