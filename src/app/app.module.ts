import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { PlatformService } from './../shared/services/platform.service';
import { SocialMetaService } from './../shared/services/social-meta.service';

import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'serverApp' }),
    HttpClientModule
  ],
  providers: [
    PlatformService,
    SocialMetaService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
