import { Injectable } from '@angular/core';
import { Meta } from '@angular/platform-browser';

@Injectable()
export class SocialMetaService {
  constructor(private meta: Meta) {}
  
  public setFacebookMeta(obj: any) {
    console.log('OBJ', obj);
    this.meta.addTags([
      {
        property: 'og:image',
        // content: `http://157.230.32.82:4000/assets/tunaiku-high-res.png`,
        content: `${obj.imagePath}`
      },

      {
        property: 'og:image:secure_url',
        content: `${obj.imagePath}`,
      },
      { property: 'og:type', content: 'article' },
      { property: 'og:url', content: `http://157.230.32.82:4000` },
      { property: 'og:site_name', content: 'Beta Testing Trevor' },
      {
        property: 'og:title',
        content: 'Beta Testing Trevor : Yayasan Anak Papua'
      },
      {
        property: 'og:description',
        content: 'Trevor adalah yayasan perlindungan anak papua. Disini kami membantu memberikan pendidikan terbaik untuk anak-anak di papua.'
      },
    ]);
  }
}